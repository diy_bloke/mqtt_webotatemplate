void tijd() {
  //time
  //dateTime = NTPch.getNTPtime(1.0, 1); from NTP server
  dateTime = NTPch.getTime(1.0, 1); // get time from internal clock
  if (dateTime.valid) {
    //NTPch.printDateTime(dateTime);
    actualHour = dateTime.hour;
    actualMinute = dateTime.minute;
    actualsecond = dateTime.second;
    actualyear = dateTime.year;
    actualMonth = dateTime.month;
    actualday = dateTime.day;
    actualdayofWeek = dateTime.dayofWeek;
  }
  actualdayofYear =doy();
  //Serial.print("DOY ");
  //Serial.println(actualdayofYear);
  String payload="{\"Uur\":"+ String(actualHour)+",\"Min\":"+String(actualMinute)+",\"Mo\":"+String(actualMonth)+",\"Jaar\":"+String(actualyear)+",\"Dag\":"+String(actualday)+",\"Dow\":"+String(actualdayofWeek)+"}";
  //String payload="{\"Uur\":"+ String(actualHour)+",\"Min\":"+String(actualMinute)+",\"Mo\":"+String(actualMonth)+",\"Jaar\":"+String(actualyear)+",\"Dag\":"+String(actualday)+",\"Dow\":"+String(actualdayofWeek)+",\"Doy\":"+String(actualdayofYear)+"}";
  
  payload.toCharArray(data, (payload.length() + 1));

  client.publish("home/template/stat/Tijd", data);
  client.publish("home/template/stat/doy",String(actualdayofYear).c_str());
}


//the doy function adds a day in regular leap years, but not in century leapyers. 
//the function gives a false result every 400 years (so in year 2400), as it does not add a leapday then
int doy()
{
d=actualday;//actualday
//Serial.print(d);
for (int i=0;i<actualMonth-1;i++){
d=d+daysInMonth[i];
//Serial.println(d);
}
if (actualMonth > 2 && actualyear %4==0 && actualyear %100 !=0)
{d=d+1;}
return d;
}