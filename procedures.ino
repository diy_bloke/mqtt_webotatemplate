void systeem() {

  uint32_t realSize = ESP.getFlashChipRealSize();
  uint32_t ideSize = ESP.getFlashChipSize();
  String ideSizeS = String(ideSize);

  FlashMode_t ideMode = ESP.getFlashChipMode();
  uint32_t ideID = ESP.getFlashChipId();
  String ideIDs = String(ESP.getFlashChipId(), HEX);
  //add leading zeros
  byte idegap = 8 - ideIDs.length();
  ideIDs.toUpperCase();
  //Serial.println(idegap);
  for (i = 0; i < idegap; i++)
  {
    ideIDs = "0" + ideIDs;
  }
  //https://arduino-esp8266.readthedocs.io/en/latest/libraries.html
  String core = ESP.getCoreVersion();
  client.publish("home/template/stat/core", core.c_str());
  String sdk = String(ESP.getSdkVersion());
  client.publish("home/template/stat/sdk", sdk.c_str());
  String reason = ESP.getResetReason();
  client.publish("home/template/stat/reeason", reason.c_str());
  uint8_t fragmentation = ESP.getHeapFragmentation();
  client.publish("home/template/stat/fragmentation", String(fragmentation).c_str());
  uint32_t cyclecount = ESP.getCycleCount();
  String cyclecountS = String(ESP.getCycleCount(), HEX);
  cyclecountS.toUpperCase();
  client.publish("home/template/stat/cyclecount", String(cyclecount).c_str());
  //client.publish("home/template/stat/cyclecounthex",String(ESP.getCycleCount(),HEX).toUpperCase().c_str());
  client.publish("home/template/stat/cyclecounthex", cyclecountS.c_str());


  //Serial.printf("Flash real id:   %08X\n", ESP.getFlashChipId());
  //Serial.printf("Flash real size: %u bytes\n\n", realSize);

  //Serial.printf("Flash ide  size: %u bytes\n", ideSize);
  //Serial.printf("Flash ide speed: %u Hz\n", ESP.getFlashChipSpeed());
  //Serial.printf("Flash ide mode:  %s\n", (ideMode == FM_QIO ? "QIO" : ideMode == FM_QOUT ? "QOUT" : ideMode == FM_DIO ? "DIO" : ideMode == FM_DOUT ? "DOUT" : "UNKNOWN"));
  String idemode = ("Flash ide mode:  %s\n", (ideMode == FM_QIO ? "QIO" : ideMode == FM_QOUT ? "QOUT" : ideMode == FM_DIO ? "DIO" : ideMode == FM_DOUT ? "DOUT" : "UNKNOWN"));
  client.publish("home/template/stat/idemode", idemode.c_str());
  if (ideSize != realSize) {
    //Serial.println("Flash Chip configuration wrong!\n");
  } else {
    //Serial.println("Flash Chip configuration ok.\n");
  }


  client.publish("home/template/stat/ChipID", ideIDs.c_str());
  client.publish("home/template/stat/memory", ideSizeS.c_str());
  client.publish("home/template/stat/realmem", String(realSize).c_str());
}

//Get the filename
void bestandsnaam()
{
//strip path van filenaam
  byte p1 = naam.lastIndexOf('\\');
  byte p2 = naam.lastIndexOf('.');
  naam = naam.substring(p1 + 1, p2);
  
    Serial.print("Filenaam: ");
    Serial.println(naam);
  }
  
  
void sendIPetc()
{
  for (i = 0; i < 16; i++) {
    buff_msg[i] = IP[i];
  }
  buff_msg[i] = '\0';
  client.publish("home/template/stat/IP", buff_msg );
  //rssi
  signalStrength = WiFi.RSSI();
  sprintf(buff_msg, "%d", signalStrength);
  client.publish("home/template/stat/RSSI", buff_msg );
  //-mac
  for (i = 0; i < 17; i++) {
    buff_msg[i] = MAC[i];
  }
  buff_msg[i] = '\0';
  client.publish("home/template/stat/MAC", buff_msg);
  client.publish("home/template/stat/naam", naam.c_str());
}
  