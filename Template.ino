/*
  Basic ESP8266 MQTT example
  This sketch demonstrates the capabilities of the pubsub library in combination
  with the ESP8266 board/library.
  It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off
  It will reconnect to the server if the connection is lost using a blocking
  reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
  achieve the same result without blocking the main loop.
  To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"
*/

#include <ESP8266WiFi.h> //for MQTT
#include <PubSubClient.h> //for MQTT
#include <ESP8266httpUpdate.h>

// Update these with values suitable for your network.

const char* ssid = "mysecretSSID";//Your SSID
const char* password = "mysecretPW";//Your password
const char* mqtt_server = "192.168.x.yyy";//our MQTT broker IP

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
String naam = (__FILE__);     // filenaam
String IP;                    // IPaddress of ESP
String MAC;
int   signalStrength;
char buff_msg[22];            // mqtt message
char data[80];
byte i;
byte heartbeat = 0;
long  lastMinute = -1;        // timestamp last minute
long  upTime = 0;             // uptime in minutes
byte TIMinterval = 20;// max 255=4.25 min
//-------for sntp
// https://github.com/SensorsIot/SNTPtime
#include <SNTPtime.h>
SNTPtime NTPch("nl.pool.ntp.org");//pick time derver of your choosing
strDateTime dateTime;
//for timeserver
byte actualHour;
byte actualMinute;
byte actualsecond;
int actualyear;
byte actualMonth;
byte actualday;
byte actualdayofWeek;
//int dayofyear;
int d;
uint16_t actualdayofYear;//actualdayofYear
const uint8_t daysInMonth [] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

//--------------
//Static IP address configuration
/*
  IPAddress ip(192,168,1,30);//StaticIP of your choice, outside your DHCP range
  IPAddress gateway(192,168,1,1);
  IPAddress subnet(255,255,255,0);
  //IPAddress dns(192,168,1,1);  //DNS
*/
//----------------

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
//WiFi.config(ip,gateway,subnet);// uncomment for static ip
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    //Serial.print((char)payload[i]);
  }
  //-------get your payload
  String boodschap;
  for (uint8_t i = 0; i < length; i++) {
    boodschap.concat((char)payload[i]);
  }
  Serial.println(boodschap);
  //-----



  //****************** SET INTERVAL ******************
  if (String("home/template/cmd/interval").equals(topic))
  {
    TIMinterval = boodschap.toInt();
    //if (TIMinterval < 5 && TIMinterval != 0) TIMinterval = 5; // minimum interval is 5 seconds
    if (TIMinterval < 5) TIMinterval = 5; // minimum interval is 5 seconds
    client.publish("home/template/tele/interval", String(TIMinterval).c_str());
  }
  Serial.println(TIMinterval);
  
  //-----------------
  // Switch on the LED if an 1 was received as first character. reacts on any subscription
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

  //--------------------Update the firmware
  if (String("home/template/cmd/update").equals(topic))
  {
    firmwareUpdate();
  }
  //---------------------Switch something on or off
  if (String("home/template/cmd/light").equals(topic))
  {
    if (boodschap.equals("ON")) {
      Serial.println("put your device on command here");
    }

    if (boodschap.equals("OFF")) {
      Serial.println("Put your device off command here");
    }
  }
  //---------------------
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("inTopic");
      client.subscribe("home/template/cmd/#");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as OUTPUT
  MAC = WiFi.macAddress();
  bestandsnaam(); //filename
  Serial.begin(115200);
  wifi_station_set_hostname("Template");//DHCP name
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  IP = WiFi.localIP().toString();
  //for SNTP
  while (!NTPch.setSNTPtime());
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - lastMsg > TIMinterval * 1000) {
    lastMsg = now;
    ++value;
    snprintf (msg, 50, "hello world #%ld", value);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("outTopic", msg);
    client.publish("home/template/tele/description", "Template with WebOTA");
    sendIPetc();
    client.publish("home/template/tele/heartbeat", String(heartbeat).c_str());
    heartbeat = heartbeat + 1;
    systeem();
    client.publish("home/template/tele/uptime", String(upTime).c_str());
    client.publish("home/template/tele/interval", String(TIMinterval).c_str());
    client.publish("home/template/tele/voltage", String((analogRead(A0) / 1023.0) * 4.2).c_str());
    tijd();
    client.publish("home/template/status/update", "standby");
    client.publish("home/template/tele/baud", String(Serial.baudRate()).c_str());

  }
  //-----
  // Increase upTime

  if (lastMinute != (millis() / 60000)) {         // another minute passed ?
    lastMinute = millis() / 60000;
    upTime++;
  }
  //----
}

void firmwareUpdate()
{
  Serial.println("updating");
  String d="updating "+String(naam);
  client.publish("home/template/status/update", String(d).c_str());
  
  t_httpUpdate_return ret = ESPhttpUpdate.update("http://192.168.x.yyy/update/+String(naam)+".ino.d1_mini.bin");//set up your update server
//xxxxxx.ino.d1_mini.bin is generated for arduino d1 mini
//xxxxxx.ino.generic.bin is generated for ESP8266-01
//However, any (bin) file name may be chosen
  switch (ret) {
    case HTTP_UPDATE_FAILED:
      Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
      break;
    case HTTP_UPDATE_NO_UPDATES:
      Serial.println("HTTP_UPDATE_NO_UPDATES");
      break;
    case HTTP_UPDATE_OK:
      Serial.println("HTTP_UPDATE_OK");//There will be a reset before this is printed
      break;
  }
}